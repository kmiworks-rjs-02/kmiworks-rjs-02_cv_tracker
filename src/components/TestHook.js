import React, { useState } from 'react';

const TestHook = () => {
    const [count, setCount] = useState(0);

    const clickHandler = () => {
        //        setCount(count + 1);
        setCount((prevCount) => prevCount + 1);
    };

    return (
        <div>
            <h2>Count: {count}</h2>
            <button onClick={clickHandler}>Click</button>
        </div>
    );
};
export default TestHook;
