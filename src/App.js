import React, { Component } from 'react';

// import Cards from './components/Cards/Cards';
// import Chart from './components/Chart/Chart';
// import CountryPicker from './components/CountryPicker/CountryPicker';

import { Cards, Chart, CountryPicker } from './components';
import styles from './App.module.css';
import { fetchData } from './api';

// import covid19Image from './images/image.png';

// import TestHook from './components/TestHook';

class App extends Component {
    state = {
        data: {},
        country: '',
    };

    async componentDidMount() {
        const fetchedData = await fetchData();

        this.setState({ data: fetchedData });
        // console.log(data);
    }

    handleCountryChange = async (country) => {
        const fetchedData = await fetchData(country);
        // console.log(fetchedData);
        this.setState({ data: fetchedData, country: country });
    };

    render() {
        const { data, country } = this.state;
        return (
            <div className={styles.container}>
                {/* <img */}
                {/*     className={styles.image} */}
                {/*     src={covid19Image} */}
                {/*     alt="COVID-19" */}
                {/* /> */}
                <h1 className={styles.title}>COVID19-Tracker</h1>
                <Cards data={data} />
                <CountryPicker handleCountryChange={this.handleCountryChange} />
                <Chart data={data} country={country} />
                {/* <TestHook /> */}
            </div>
        );
    }
}

export default App;
